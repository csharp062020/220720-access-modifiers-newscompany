﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsCompany;

namespace Store
{
    public class Seller
    {
        private float _moneyEarned = 0;

        public void SellNewspaper(Newspaper newspaper)
        {
            _moneyEarned += newspaper.GetPrice();
        }

        public override string ToString()
        {
            return $"{_moneyEarned}";
        }
    }
}