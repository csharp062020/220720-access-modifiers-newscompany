﻿namespace NewsCompany
{
    internal class PoliticsTeam : NewsTeam
    {

        internal override void AddStyle()
        {
            this._story._visualStyle = "Style PoliticsTeam";
        }
        internal void AddStyle(string style)
        {
            this._story._visualStyle = style;
        }


        internal override void CreatStory()
        {
            _story = new Story();
            _story._title = "PoliticsTeam Title";
            _story._body = "PoliticsTeam Body"; 
        }
        internal void CreatStory(string title, string body)
        {
            _story = new Story();
            _story._title = title;
            _story._body = body;
        }
    }

}
