﻿namespace NewsCompany
{
    public class Story
    {
        public string _title;
        public string _body;
        public string _visualStyle;

        public Story()
        {
        }
        public Story(string title , string body, string visualStyle) // for test only 
        {
            _title = title;
            _body = body;
            _visualStyle = visualStyle;
        }

        public override string ToString()
        {
            return $"{base.ToString()} title: {_title} , body: {_body}, visual style {_visualStyle}";
        }

    }

}
