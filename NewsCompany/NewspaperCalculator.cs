﻿namespace NewsCompany
{

    public static class NewspaperCalculator
    {
        public static int CalcNumberOfChars(Newspaper newspaper)
        {
            int sumOfCharOnNewspaper=0; 

            foreach(Story story in newspaper.GetStories())
            {
                sumOfCharOnNewspaper += story._body.Length;
                sumOfCharOnNewspaper += story._title.Length;
            }

            return sumOfCharOnNewspaper; 
        }
    }

}
