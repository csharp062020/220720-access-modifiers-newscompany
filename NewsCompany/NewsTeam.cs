﻿namespace NewsCompany
{
    internal abstract class NewsTeam
    {
        protected Story _story;

        internal abstract void CreatStory();

        internal virtual void AddStyle()
        {

        }

        internal Story GetStory()
        {
            return _story;
        }

        public override string ToString()
        {
            return $"{base.ToString()} , story {_story}";
        }
    }

}
