﻿using System;

namespace NewsCompany
{
    public class Newspaper
    {
        private const int MAX_STORIES = 3;
        private Story[] _stories;
        private float _price;

        public Story[] GetStories()
        {
            return _stories;
        }

        public void SetStories (Story[] stories)
        {
            if (MAX_STORIES >= stories.Length)
            {
                _stories = stories;
            }
        }

        public float GetPrice()
        {
            return _price;
        }

        public void SetPrice(float price)
        {
            if (price > 0.0 )
            {
                _price = price;
            }
        }
        public void Read()
        {
            Console.WriteLine($"we have {_stories.Length} stories:");
            foreach(Story story in _stories)
            {
                Console.WriteLine(story.ToString());
            }
        }


        public override string ToString()
        {
            return $"{base.ToString()} price: {_price} , stories {_stories.Length}";
        }
    }
}
