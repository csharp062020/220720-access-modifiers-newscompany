﻿namespace NewsCompany
{
    internal class HealthTeam : NewsTeam
    {
       
        internal override void AddStyle()
        {
            this._story._visualStyle = "Style HealthTeam";
        }
        internal void AddStyle(string style)
        {
            this._story._visualStyle = style;
        }

        internal override void CreatStory()
        {
            _story = new Story();
            _story._title = "HealthTeam Title";
            _story._body = "HealthTeam Body";
        }
        internal void CreatStory(string title, string body)
        {
            _story = new Story();
            _story._title = title;
            _story._body = body;
        }
    }

}
