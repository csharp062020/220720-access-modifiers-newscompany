﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsCompany;
using Store;

namespace hw_22072020
{
    class Program
    {
        static void Main(string[] args)
        {
            Story[] stories = new Story[3];

            Newspaper SunDay = new Newspaper();
            Newspaper MonDay = new Newspaper();
            Seller Store1 = new Seller();

            stories[0] = new Story("bla", "lala", "david");
            stories[1] = new Story("bla1", "lala", "david");
            stories[2] = new Story("bla2", "lala2", "david");

            SunDay.SetStories(stories);
            SunDay.SetPrice(5.5f);
            Console.WriteLine($"Sunday CalcNumberOfChars: {NewspaperCalculator.CalcNumberOfChars(SunDay)}");
            SunDay.Read();

            stories[2]._body += "2";
            MonDay.SetStories(stories);
            MonDay.SetPrice(10.8f);
            Console.WriteLine($"MonDay CalcNumberOfChars: {NewspaperCalculator.CalcNumberOfChars(MonDay)}");
            MonDay.Read();

            Store1.SellNewspaper(SunDay);
            Store1.SellNewspaper(SunDay);
            Store1.SellNewspaper(MonDay);

            Console.WriteLine($"sales: {Store1.ToString()} ");
        }
    }
}

